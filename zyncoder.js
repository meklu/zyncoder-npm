const Zyncoder = {
	Z: () => ['z', 'Z', 'з', 'З'],
	Y: () => ['y', 'Y', 'ы', 'Ы'],
	N: () => ['n', 'N', 'н', 'Н'],

	zynchars: () => Zyncoder.Z().concat(Zyncoder.Y()).concat(Zyncoder.N()),

	zyntable: () => {
		const Z = Zyncoder.Z();
		const Y = Zyncoder.Y();
		const N = Zyncoder.N();

		var t, c, z, y, n;
		t = [];
		c = 0;
		for (z = 0; z < Z.length; z += 1) {
			for (y = 0; y < Y.length; y += 1) {
				for (n = 0; n < N.length; n += 1) {
					t[c] = Z[z] + Y[y] + N[n];
					c += 1;
				}
			}
		}

		return t;
	},

	zynrtable: () => {
		const Z = Zyncoder.Z();
		const Y = Zyncoder.Y();
		const N = Zyncoder.N();

		var t, c, z, y, n;
		t = {};
		c = 0;
		for (z = 0; z < Z.length; z += 1) {
			for (y = 0; y < Y.length; y += 1) {
				for (n = 0; n < N.length; n += 1) {
					t[Z[z] + Y[y] + N[n]] = c;
					c += 1;
				}
			}
		}

		return t;
	},

	trip2zyn: (triplet) => {
		var len = triplet.length;
		var triplet = triplet.concat([0, 0, 0]);
		var lkup = Zyncoder.zyntable();
		var out = [];
		out.push( lkup[(triplet[0] & 0xFC) >> 2] );
		out.push( lkup[((triplet[0] & 0x03) << 4) | ((triplet[1] & 0xF0) >> 4)] );
		if (len >= 2) {
			out.push( lkup[((triplet[1] & 0x0F) << 2) | ((triplet[2] & 0xC0) >> 6)] );
			if (len >= 3) {
				out.push( lkup[triplet[2] & 0x3F] );
			} else {
				out[out.length - 1] += ")";
			}
		} else {
			out[out.length - 1] += "))";
		}
		return out;
	},

	zyn2trip: (zyn) => {
		zyn = zyn.filter((v) => (v === ")") ? false : true);
		var len = zyn.length;
		var lkup = Zyncoder.zynrtable();
		var zn = [];
		zn = zyn.map((z) => lkup[z]);
		zn = zn.concat([0, 0, 0]);
		out = [];
		out.push( String.fromCharCode( ((zn[0] & 0x3F) << 2) | ((zn[1] & 0x30) >> 4) ) );
		if (len >= 3) {
			out.push( String.fromCharCode( ((zn[1] & 0x0F) << 4) | ((zn[2] & 0x3C) >> 2) ) );
			if (len >= 4) {
				out.push( String.fromCharCode( ((zn[2] & 0x03) << 6) | (zn[3] & 0x3F) ) );
			}
		}
		return out;
	},

	rEnc: (bs) => {
		var lkup = Zyncoder.zyntable();
		var acc = [];
		var out = [];
		var pos;
		for (pos = 0; pos < bs.length; pos += 1) {
			b = bs[pos].charCodeAt(0);
			acc.push(b);
			if (acc.length === 3) {
				Zyncoder.trip2zyn(acc).forEach((o) => {
					out.push( o );
				});
				acc = [];
			}
		}
		if (acc.length > 0) {
			Zyncoder.trip2zyn(acc).forEach((o) => {
				out.push( o );
			});
		}

		return out.join(" ");
	},

	rDec: (bs, harsh = false) => {
		var acc = [];
		var zyns = [];
		var out = [];

		var ws = " \t\n\r";
		var ck = [Zyncoder.Z(), Zyncoder.Y(), Zyncoder.N()];
		var cnt = 0;

		var pos;
		for (pos = 0; pos < bs.length; pos += 1) {
			c = bs[pos];
			if (!ck[cnt % 3].includes(c)) {
				if (harsh === true && ws.includes(c) === false) {
					if (c !== ")" || bs.length - pos > 2) {
						return false;
					}
				}
				continue;
			}
			cnt += 1;
			acc.push (c);
			if (acc.length === 3) {
				zyns.push(acc.join(""));
				acc = [];
			}
			if (zyns.length === 4) {
				Zyncoder.zyn2trip(zyns).forEach((o) => {
					out.push( o );
				});
				zyns = [];
			}
		}
		if (harsh === true && acc.length > 0) {
			return false;
		}
		if (zyns.length > 0) {
			Zyncoder.zyn2trip(zyns).forEach((o) => {
				out.push( o );
			});
		}
		if (harsh === true) {
			var n = 4 - zyns.length;
			if (n > 2) {
				return false;
			}
			if (bs.substr(-n) !== "))".substr(-n)) {
				return false;
			}
		}
		return out.join("");
	},

	enc: (s) => {
		return Zyncoder.rEnc(encodeURIComponent(s).replace(/%([0-9A-F]{2})/g, (_, x) =>
			String.fromCharCode('0x' + x)
		));
	},

	dec: (s) => {
		return decodeURIComponent(Zyncoder.rDec(s).split('').map((c) =>
			'%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
		).join(''));
	}
};

module.exports = Zyncoder;
