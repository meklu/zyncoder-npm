# zyncoder

Allows you to zyncode things with a custom base64 -like system.

## Usage

    const zyncoder = require("zyncoder")
    // encode a message
    zyncoder.enc("zyn:D")
    // decode a message
    zyncoder.dec("ZыN зYн ЗЫN зЫН zЫн зYn Zyn)")

If you want to live dangerously, you can use `.rEnc()` and `.rDec()` instead.
Keep in mind though that those methods do not handle UTF-8 properly, at least
not in the browser.

## WTF?

There's an interactive demo now available at https://zyn.pilipali.io/zyncoder/
